# Create Remote User
WordPress plugin to manage users on a remote install

## Dependencies

This plugin works in conjuntion with [JSON API](https://wordpress.org/plugins/json-api/) and [JSON API User](https://wordpress.org/plugins/json-api-user/) installed on a remote WordPress site.
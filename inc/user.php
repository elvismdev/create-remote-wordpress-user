<?php
/**
 * Class CruUser
 *
 * Connect and duplicate user in remote Wordpress site using JSON API User
 */
class CruUser
{
    // Define APIs
    const GET_NONCE = 'get_nonce';
    const USER_REGISTER = 'user/register';

    // Name of the plugin options
    protected $option_name = 'create-remote-user';

    // Default values
    protected $data = array(
        'url_remote_site' => 'http://site-b.dev',
        'api_base' => 'api',
        'group_id' => 0,
        'email_remote_notify' => 'no',
        'log_user_creation' => 'no',
        'email_to_notify' => 'myemail@domain.com'
        );

    public function __construct()
    {
      // if (!session_id())
      //    session_start();


       add_action( 'admin_init', array( $this, 'cru_admin_init' ) );
       add_action( 'admin_menu', array( $this, 'cru_add_page' ) );

       add_action( 'groups_created_user_group', array( $this, 'cru_register_remote' ), 10, 2 );

    // Listen for the activate event
       register_activation_hook( CRU_PLUGIN_FILE, array( $this, 'cru_activate' ) );
    // Deactivation plugin
       register_deactivation_hook( CRU_PLUGIN_FILE, array( $this, 'cru_deactivate' ) );
   }

   public function cru_activate()
   {
    update_option($this->option_name, $this->data);
}

public function cru_deactivate()
{
    delete_option($this->option_name);
}

public function cru_admin_init()
{
    register_setting('cru_options', $this->option_name, array($this, 'cru_validate'));
}

public function cru_add_page()
{
    add_options_page('Create Remote User', 'Create Remote User', 'manage_options', 'cru_options', array($this, 'cru_options_do_page'));
}

public function cru_options_do_page()
{
    $options = get_option($this->option_name);
    ?>
    <div class="wrap">
        <h2>Create Remote User Options</h2>
        <form method="post" action="options.php">
            <?php settings_fields('cru_options'); ?>
            <table class="form-table">
                <tr valign="top"><th scope="row">Remote Website URL:</th>
                    <td><input type="text" name="<?php echo $this->option_name?>[url_remote_site]" value="<?php echo $options['url_remote_site']; ?>" /></td>
                </tr>
                <tr valign="top"><th scope="row">API Base:</th>
                    <td><input type="text" name="<?php echo $this->option_name?>[api_base]" value="<?php echo $options['api_base']; ?>" /></td>
                </tr>
                <tr valign="top"><th scope="row">Group ID:</th>
                    <td><input type="text" name="<?php echo $this->option_name?>[group_id]" value="<?php echo $options['group_id']; ?>" /></td>
                </tr>
                <tr valign="top"><th scope="row">Remote Email Notify:</th>
                    <td>
                        <input name="<?php echo $this->option_name?>[email_remote_notify]" type="radio" value="yes" <?php checked( 'yes', $options['email_remote_notify'] ); ?> /> Yes
                        <br>
                        <input name="<?php echo $this->option_name?>[email_remote_notify]" type="radio" value="no" <?php checked( 'no', $options['email_remote_notify'] ); ?> /> No
                    </td>
                </tr>
                <tr valign="top"><th scope="row">Log User Creation:</th>
                    <td>
                        <input name="<?php echo $this->option_name?>[log_user_creation]" type="radio" value="yes" <?php checked( 'yes', $options['log_user_creation'] ); ?> /> Yes
                        <br>
                        <input name="<?php echo $this->option_name?>[log_user_creation]" type="radio" value="no" <?php checked( 'no', $options['log_user_creation'] ); ?> /> No
                    </td>
                </tr>
                <tr valign="top"><th scope="row">Email To Notify:</th>
                    <td><input type="text" name="<?php echo $this->option_name?>[email_to_notify]" value="<?php echo $options['email_to_notify']; ?>" /></td>
                </tr>
            </table>
            <p class="submit">
                <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
            </p>
        </form>
    </div>
    <?php
}

public function cru_validate( $input )
{
    $valid = array();
    $valid['url_remote_site'] = sanitize_text_field( $input['url_remote_site'] );
    $valid['api_base'] = sanitize_text_field( $input['api_base'] );
    $valid['group_id'] = sanitize_text_field( $input['group_id'] );
    $valid['email_remote_notify'] = sanitize_text_field( $input['email_remote_notify'] );
    $valid['log_user_creation'] = sanitize_text_field( $input['log_user_creation'] );
    $valid['email_to_notify'] = sanitize_email( $input['email_to_notify'] );
    if (strlen($valid['url_remote_site']) == 0) {
        add_settings_error(
    'url_remote_site',                     // Setting title
    'urlremotesite_texterror',            // Error ID
    'Please enter a valid URL',     // Error message
    'error'                         // Type of message
    );
    // Set it to the default value
        $valid['url_remote_site'] = $this->data['url_remote_site'];
    }
    if ( strlen( $valid['api_base'] ) == 0 ) {
        add_settings_error(
            'api_base',
            'apisecret_texterror',
            'Please specify the API base',
            'error'
            );
        $valid['api_base'] = $this->data['api_base'];
    }
    if ( strlen( $valid['group_id'] ) ==  0) {
        add_settings_error(
            'group_id',
            'groupid_texterror',
            'Please specify the Group',
            'error'
            );
        $valid['group_id'] = $this->data['group_id'];
    }
    if ( strlen( $valid['email_remote_notify'] ) == 0 ) {
        add_settings_error(
            'email_remote_notify',
            'emailremotenotify_texterror',
            'Please select to notify user from remote site',
            'error'
            );
        $valid['email_remote_notify'] = $this->data['email_remote_notify'];
    }
    if ( strlen( $valid['log_user_creation'] ) == 0 ) {
        add_settings_error(
            'log_user_creation',
            'debugusercreation_texterror',
            'Please select if you would like to debug the user creation',
            'error'
            );
        $valid['log_user_creation'] = $this->data['log_user_creation'];
    }
    if ( strlen( $valid['email_to_notify'] ) == 0 ) {
        add_settings_error(
            'email_to_notify',
            'emailtonotify_texterror',
            'Please specify an email to send logs about user creation',
            'error'
            );
        $valid['email_to_notify'] = $this->data['email_to_notify'];
    }
    return $valid;
}



/**
 * @param string $status
 * @param string $message
 *
 * Set a proper description to it
 */
// public function cru_html_notice($status = 'updated', $message = '')
// {
//     if (!$message)
//         return;

//     echo '<div class="' . $status . ' notice is-dismissible"><p>' . $message . '</p>
//     <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
// </div>';
// }

/**
 * Set a proper description to it
 */
// public function cru_notice()
// {

//     add_action('admin_notices', array($this, 'cru_html_notice'), 10, 2);
//     do_action('admin_notices', $_SESSION['notify']['class'], $_SESSION['notify']['message']);

//     unset($_SESSION['notify']);

// }

/**
* @param string $status
* @param mixed $response
*
*/
public function cru_notify( $status, $response, $user_email, $settings )
{
    switch ($status) {
        case 'success':
        $message = 'User created remotely';
        break;
        case 'error':
        $message = 'Some error ocurred';
        if (method_exists($response, 'get_error_message'))
            $message = $response->get_error_message();
        elseif (isset($response->error))
            $message = $response->error;
        break;
        default:
        $message = 'Some default message';
        break;
    }
    // $_SESSION['notify'] = array('class' => $status, 'message' => $message);
    strtoupper($status);
    $message = $message . ": $user_email";
    if ( $settings['log_user_creation'] == 'yes' )
        wp_mail( $settings['email_to_notify'], "Create Remote User: Duplication $status", $message );
}



// CORE FUNCTIONS
/**
     * @param $user_id
     * @param $group_id
     *
     * Set a proper description to it
     */
public function cru_register_remote($user_id, $group_id)
{
    $settings = get_option('create-remote-user');

    if ( $group_id == $settings['group_id'] ) {

        $user_data = get_userdata($user_id);

            // The args for the nounce
        $params = array(
            'controller' => 'user',
            'method' => 'register'
            );

            // Generate the URL for the nounce
        $api_url = $settings['url_remote_site'];
        if ( substr( $api_url, -1 ) != '/' )
            $api_url .= '/';

        $api_url .= $settings['api_base'];
        if ( substr( $api_url, -1 ) != '/' )
            $api_url .= '/';

        $url = $api_url . self::GET_NONCE;
        $url = add_query_arg( $params, esc_url_raw( $url ) );

        // error_log($url);

            // Make API request
        $get_nonce_response = wp_remote_get( esc_url_raw( $url ) );
        $decoded_response = json_decode( wp_remote_retrieve_body( $get_nonce_response ) );

        if ( is_wp_error( $get_nonce_response) || $decoded_response->status == 'error' ) {
            $this->cru_notify( 'error', $decoded_response, $user_data->user_email, $settings );

        } else {

            // The args to create user
            $params = array(
                'nonce' => $decoded_response->nonce,
                'username' => urlencode( $user_data->user_login ),
                'email' => urlencode( $user_data->user_email ),
                'display_name' => $user_data->display_name,
                'first_name' => $user_data->first_name,
                'last_name' => $user_data->last_name,
                'user_pass_hash' => urlencode( $user_data->user_pass ),
                'notify' => $settings['email_remote_notify']
                );

            // Generate the URL to create user
            $url = $api_url . self::USER_REGISTER;
            $url = add_query_arg( $params, esc_url_raw( $url ) );

            // error_log($url);

            $create_user_response = wp_remote_get( esc_url_raw( $url ) );
            $decoded_response = json_decode( wp_remote_retrieve_body( $create_user_response ) );

            if ($decoded_response->status == 'ok')
                $this->cru_notify( $status = 'success', $response = null, $user_data->user_email, $settings );
            else
                $this->cru_notify( 'error', $decoded_response, $user_data->user_email, $settings );
        }
    }
}

}
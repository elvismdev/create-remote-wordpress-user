<?php
/**
 * Plugin Name: Create Remote User [Groups]
 * Plugin URI: https://bitbucket.org/grantcardone/create-remote-wordpress-user
 * Description: Duplicates user on a remote WordPress install when added to specific group.
 * Version: 1.1.3
 * Author: Elvis Morales
 * Author URI: https://twitter.com/n3rdh4ck3r
 * Requires at least: 3.5
 * Tested up to: 4.1
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

define('CRU_PLUGIN_FILE', __FILE__);
define( 'CRU_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

require_once( CRU_PLUGIN_PATH . 'inc/user.php' );

// if (is_admin()) {
$api = new CruUser();
	// if (isset($_SESSION['notify']))
	// 	$api->cru_notice();
// }